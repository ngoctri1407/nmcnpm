<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name("main");
Route::get('/login', 'HomeController@index')->name("main");
Route::get('/logout', 'HomeController@logout')->name("main");
Route::post('/', 'HomeController@login');

Route::get('/detail/{id}', 'HomeController@detail');

Route::get('/register', 'HomeController@register')->name("register");
Route::post('/register', 'HomeController@register')->name("register");

Route::get('/search', 'HomeController@search');
Route::get('/contact', 'HomeController@contact');

Route::get('/admin', 'AdminController@index');
Route::post('/admin/login', 'AdminController@login');
Route::get('/admin/new', 'AdminController@newBlog');
Route::post('admin/news/manage', 'AdminController@manage');
Route::get('admin/delete/{id}', 'AdminController@manage');

Route::get('/admin/categories', 'AdminController@category');
Route::post('/admin/addCategory', 'AdminController@addCategory');
Route::post('/admin/editCategory', 'AdminController@editCategory');
Route::get('/admin/delCategory/{id}', 'AdminController@delCategory');

Route::get('/admin/youtube', 'AdminController@youtube');
Route::post('/admin/addYoutube', 'AdminController@addYoutube');
Route::post('/admin/editYoutube', 'AdminController@editYoutube');
Route::get('/admin/delYoutube/{id}', 'AdminController@delYoutube');