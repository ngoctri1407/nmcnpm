<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Account;
use Session;
use App\User;
use File;
class AdminController extends Controller
{
	  public function index()
    {
        if(Session::has('usernamelogin'))
        {
          if(Session::get('usernamelogin')->role == 'admin'){
            $lstBlogs = \App\Blogs::where('author', '=',Session::get('usernamelogin')->id )->paginate(20);
            return view('admin.index')->with('lstBlogs',$lstBlogs);
          }
          elseif (Session::get('usernamelogin')->role == 'user') {
           return redirect('/');
          }
        }
        else
        {
            return view('admin.login');    
        }
    }
    public function newBlog(){
        $categories = \App\Categories::all();
        return view('admin.newBlogs')->with('categories',$categories);
    }
    public function manage(Request $request){
        if (Session::has('usernamelogin')) {
                if($request->action == "Lưu") {
                    if ($request->id == 0) {
                        $news = new \App\Blogs;
                        $news->id = $request->id;
                        $news->title = $request->title;
                        $news->detail = $request->content;
                        $news->description = $request->short_description;
                        $news->category = $request->category_id;
                        $news->author = Session::get('usernamelogin')->id;
                        $news->save();
                        if ($request->hasFile('photo')) {
                            $file = $request->file('photo');
                            $savePath = 'uploads/blogs/';
                            $fileName = time() . '_' . $file->getClientOriginalName();
                            $file = $file->move(public_path().'/'.$savePath, $fileName);
                            $image = new \App\Images;
                            $image->path = $savePath . $fileName;
                            $image->blog_id = $news->getKey();
                            $image->save();
                        }
                    } else {
                        $news = \App\Blogs::where('id', '=', $request->id)->first();
                        $news->title = $request->title;
                        $news->detail = $request->content;
                        $news->description = $request->short_description;
                        $news->category = $request->category_id;
                        $news->author = Session::get('usernamelogin')->id;
                        $news->save();
                        if ($request->hasFile('photo')) {
                            $oldImage = \App\Images::where('blog_id', '=', $request->id)->first();
                            if($oldImage != null){
                                if (File::exists((public_path() . '/' . $oldImage->path))) {
                                unlink(public_path() . '/' . $oldImage->path);
                                }
                                $file     = $request->file('photo');
                                $savePath = 'uploads/blogs/';
                                $fileName = time() . '_' . $file->getClientOriginalName();
                                $file     = $file->move(public_path() . '/' . $savePath, $fileName);
                                $oldImage->path = $savePath . $fileName;
                                $oldImage->save();
                            }
                            else {
                                $file = $request->file('photo');
                                $savePath = 'uploads/blogs/';
                                $fileName = time() . '_' . $file->getClientOriginalName();
                                $file = $file->move(public_path().'/'.$savePath, $fileName);
                                $image = new \App\Images;
                                $image->path = $savePath . $fileName;
                                $image->blog_id = $news->getKey();
                                $image->save();
                            }
                        }
                    }
                }
                else {
                    $news = \App\Blogs::where('id', '=', $request->id)->first();
                    $news->delete();
                }
                return redirect('/admin');
        }
        return redirect('/admin');
    }

    public function login(Request $request)
    {
        $account = \App\User::where('email','=',$request->email)->where('password','=',MD5($request->password))->first();
        if(!$account)
        {
            $error = "Username or Password is incorrect";
            return view("admin.login")->with('error',$error);
        }
        else
        {
            Session::put('usernamelogin',$account);
            if(Session::has('usernamelogin'))
            return redirect('/admin');
        }
    }
    public function register(Request $request)
    {
      if ($request->isMethod('post')) {
        if(!$request->password || !$request->confirm_password || ($request->password != $request->confirm_password)){
            $error = "Password is incorrect! Please try again";
            return view("home.register")->with('error',$error);
        }
        $user = \App\User::where('email','=',$request->email)->first();
        if($user){
            $error = "Email has been used. Please try another email !";
            return view("home.register")->with('error',$error);
        }
        $user = new User;
        $user->email = $request->email;
        $user->password = MD5($request->password);
        $user->name = $request->name;
        $user->role = "user";
        $user->save();
        Session::put('usernamelogin',$user);
        return view('home.successRegister');
      }
      else{
        return view('home.register');
      }
    }
    public function category(){
        $categories = \App\Categories::paginate(20);
        return view('admin.categories')->with('categories',$categories);
    }
    public function addCategory(Request $request){
        $category = new \App\Categories;
        $category->name = $request->name;
        if ($request->hasFile('photo')) {
                            $file = $request->file('photo');
                            $savePath = 'uploads/category/';
                            $fileName = time() . '_' . $file->getClientOriginalName();
                            $file = $file->move(public_path().'/'.$savePath, $fileName);
                            $category->img = $savePath . $fileName;
                        }
        else{
           $category->img = 'noImg';
        }
        $category->type = $request->type;
                $category->save();
        return redirect('admin/categories');
    }
    public function editCategory(Request $request){
        $category = \App\Categories::where('id',$request->id)->first();
        $category->name = $request->name;



        if ($request->hasFile('photo')) {
           if (File::exists((public_path() . '/' . $category->img))) {
                unlink(public_path() . '/' . $category->img);
            }
            $file = $request->file('photo');
            $savePath = 'uploads/category/';
            $fileName = time() . '_' . $file->getClientOriginalName();
            $file = $file->move(public_path().'/'.$savePath, $fileName);
            $category->img = $savePath . $fileName;
        }
        $category->save();
        return redirect('admin/categories');
    }
    public function delCategory(Request $request){
        $category = \App\Categories::where('id',$request->id)->first();
       if (File::exists((public_path() . '/' . $category->img))) {
            unlink(public_path() . '/' . $category->img);
        }
        $category->delete();
        return redirect('admin/categories');
    }

    public function youtube(){
        $youtube = \App\Youtube::paginate(20);
        return view('admin.youtube')->with('youtube',$youtube);
    }
    public function addYoutube(Request $request){
        $Youtube = new \App\Youtube;
        $Youtube->link = $request->name;
        $Youtube->save();
        return redirect('admin/youtube');
    }
    public function editYoutube(Request $request){
        $Youtube = \App\Youtube::where('id',$request->id)->first();
        $Youtube->link = $request->name;
        $Youtube->save();
        return redirect('admin/youtube');
    }
    public function delYoutube(Request $request){
        $Youtube = \App\Youtube::where('id',$request->id)->first();
        $Youtube->delete();
        return redirect('admin/youtube');
    }
}
