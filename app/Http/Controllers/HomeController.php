<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Account;
use Session;
use App\User;
class HomeController extends Controller
{
  public function detail(Request $request){
    $blog = \App\Blogs::where('id',$request->id)->first();
    $img = \App\Images::where('blog_id',$blog->id)->first();
              if($img){
              $blog->img = $img->path;
              }   
              $category = \App\Categories::where('id',$blog->id)->first();
              if($category){
              $blog->category = $category->name;
              }   
              $author = \App\User::where('id',$blog->id)->first();
              if($author){
              $blog->author = $author->name;
              }
      $related = \App\Blogs::where('category',$blog->category)->take(2)->get();
              for($i = 0; $i < count($related); $i++){
            $img = \App\Images::where('blog_id',$related[$i]->id)->first();
              if($img){
              $related[$i]->img = $img->path;
              }   
              $category = \App\Categories::where('id',$related[$i]->id)->first();
              if($category){
              $related[$i]->category = $category->name;
              }   
              $author = \App\User::where('id',$related[$i]->id)->first();
              if($author){
              $related[$i]->author = $author->name;
              }      
            }
    return view('home.detail')->with('blog',$blog)->with('related',$related);
  }
  public function contact(){
    return view('home.contact');
  }
	  public function index()
    {
        if(Session::has('usernamelogin'))
        {
          $newBlog = \App\Blogs::orderBy('created_at', 'desc')->paginate(10);
          for($i = 0; $i < count($newBlog); $i++){
            $img = \App\Images::where('blog_id',$newBlog[$i]->id)->first();
              if($img){
              $newBlog[$i]->img = $img->path;
              }   
              $category = \App\Categories::where('id',$newBlog[$i]->id)->first();
              if($category){
              $newBlog[$i]->category = $category->name;
              }   
              $author = \App\User::where('id',$newBlog[$i]->id)->first();
              if($author){
              $newBlog[$i]->author = $author->name;
              }      
            }
            return view('home.index')->with('blogs',$newBlog);
        }
        else
        {
            return view('home/login');    
        }
    }
    public function login(Request $request)
    {
        $account = \App\User::where('email','=',$request->email)->where('password','=',MD5($request->password))->first();
        if(!$account)
        {
            $error = "Username or Password is incorrect";
            return view("home.login")->with('error',$error);
        }
        else
        {
            Session::put('usernamelogin',$account);
            return redirect('/');
        }
    }
    public function logout(Request $request)
    {
        $request->session()->forget('usernamelogin');
        return redirect('/');
    }
    public function changeInfo($name,$email,$passConfirm)
    {
        if(Session::has('usernamelogin'))
        {
          if(Session::get('usernamelogin')->id){
            $user = \App\User::where('id','=',Session::get('usernamelogin')->id)->where('password','=',MD5($passConfirm))->first();
            if($user == null){
              $error = 'Nhập sai mật khẩu';
              return response()->json([
                'success' => false,
                'error' => $error,
              ]);
            }
            else{
              $user->name = $name;
              $user->email = $email;
              $user->save();
              Session::forget('usernamelogin');
              Session::put('usernamelogin',$user);
              return response()->json([
                'success' => true
              ]);
            }
          }
        }
        else{
          $error = 'Chưa đăng nhập';
          return response()->json([
              'success' => false,
              'error' => $error,
          ]);
        }
    }
        public function changePass($oldPass,$newPass,$renewPass)
    {
        if(Session::has('usernamelogin'))
        {
          if(Session::get('usernamelogin')->id){
            $user = \App\User::where('id','=',Session::get('usernamelogin')->id)->where('password','=',MD5($oldPass))->first();
            if($user == null){
              $error = 'Nhập sai mật khẩu';
              return response()->json([
                'success' => false,
                'error' => $error,
              ]);
            }
            else{
              if($newPass == $renewPass){
              $user->password = MD5($newPass);
              $user->save();
              Session::forget('usernamelogin');
              Session::put('usernamelogin',$user);
              return response()->json([
                'success' => true
              ]);
              }
              else{
                $error = 'Mật khẩu mới không khớp nhau';
                return response()->json([
                  'success' => false,
                  'error' => $error,
                ]);
              }
            }
          }
        }
        else{
          $error = 'Chưa đăng nhập';
          return response()->json([
              'success' => false,
              'error' => $error,
          ]);
        }
    }
    public function register(Request $request)
    {
      if ($request->isMethod('post')) {
        if(!$request->password || !$request->confirm_password || ($request->password != $request->confirm_password)){
            $error = "Password is incorrect! Please try again";
            return view("home.register")->with('error',$error);
        }
        $user = \App\User::where('email','=',$request->email)->first();
        if($user){
            $error = "Email has been used. Please try another email !";
            return view("home.register")->with('error',$error);
        }
        $user = new User;
        $user->email = $request->email;
        $user->password = MD5($request->password);
        $user->name = $request->name;
        $user->role = "user";
        $user->save();
        Session::put('usernamelogin',$user);
        return view('home.successRegister');
      }
      else{
        return view('home.register');
      }
    }
    public function search(){
      return view('home.search');
    }
}
