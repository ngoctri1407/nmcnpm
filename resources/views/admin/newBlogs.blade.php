@extends('layouts.app') 
@section('title', 'Main page') 
@section('css')
<link href="{{ url('/').'/css/index.css' }}" rel="stylesheet"> 
<link href="{{ url('/').'/css/staff.css' }}" rel="stylesheet"> 
@endsection 
@section('content')
<style type="text/css">
    .preview-img {
    text-align: center;
    height: 300px;
}

    .preview-img img {
    height: 100%;
    max-width: 100%;
    overflow: hidden;
}
</style>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tin Tức Mới </h5>
                    </div>
                    <div class="ibox-content">
                        {!! Form::open(array('url' => '/admin/news/manage', 'files'=>true)) !!}
                        <input name="id" type="text" value="0" hidden>
                        <input name="type" type="text" value="news" hidden>
                        <input name="expired_date" value="1900/01/01" hidden>
                        <div class="form-group">
                            <label>Tiêu Đề</label>
                            <input name="title" type="text" class="form-control" required="" aria-required="true">
                        </div>
                        <div class="form-group">
                            <label>Mô Tả Ngắn</label>
                            <textarea style="min-height: 50px" name="short_description" required="" id="short_description" class="form-control"></textarea >
                        </div>
                        <div class="form-group">
                            <label>Danh Mục</label>
                            @if($categories != null)
                                <select name="category_id" class="form-control">
                                    @for($i = 0; $i < count($categories); $i++)
                                        <option value="{{ $categories[$i]->id }}">{{ $categories[$i]->name }} ({{ $categories[$i]->type }})</option>
                                    @endfor
                                </select>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Hình ảnh</label>
                            <div class="preview-img">
                                <img id="preview" src="{{asset('no_images.png')}}" alt="your image" />
                            </div>
                            <input type="file" ID="imgPre" name="photo" />
                        </div>
                        <div class="form-group">
                            <label>Nội Dung</label>
                            <textarea style="min-height: 150px" name="content" id="content" required="" class="form-control"></textarea >
                            <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                            <script>
                                CKEDITOR.replace( 'content' );
                            </script>
                        </div>
                        <div class="form-group">
                            <a href="{{ URL::to('/admin/news/list') }}" class="btn btn-white">Trở Về</a>
                            <input type="submit" name="action" class="btn btn-primary" value="Lưu">
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection 
@section('page-script')
<script src="{{ url('/').'/js/plugins/metisMenu/jquery.metisMenu.js'}}"></script>
<script src="{{ url('/').'/js/plugins/slimscroll/jquery.slimscroll.min.js'}}"></script>
<script src="{{url('/').'/js/plugins/pace/pace.min.js'}}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script>
        $( function() {
            $( "#expired_date" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        } );
    </script>
        <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgPre").change(function(){
        readURL(this);
    });
    </script>

@endsection