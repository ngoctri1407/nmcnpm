@extends('layouts.app') 
@section('title', 'Main page') 
@section('css')
<link href="{{ url('/').'/css/index.css' }}" rel="stylesheet"> 
<link href="{{ url('/').'/css/staff.css' }}" rel="stylesheet"> 
@endsection 
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách link Youtube</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    	 <div class="row">
                            <div class="col-sm-5 m-b-xs">
                            </div>
                            <div class="col-sm-4 m-b-xs">
                            </div>
                            <div class="col-sm-3">
                                <button data-toggle="modal" data-target="#addYoutube" style="float: right;" class="btn btn-info manage" data-action="new" type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span style="font-size: 13px">Thêm link Youtube</span></button>
                            </div>
                        </div>
                        <div class="modal fade" id="addYoutube" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Thêm link Youtube</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'admin/addYoutube','id' => 'addYoutube')) !!}
                          <div class="modal-body">
                            <ul>
                            <li class="info-cell">
                                <p>Link Youtube: </p>
                                <input class="form-control" name="name" type="text" required />
                            </li>
                        </ul>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button id="change" type="submit" class="btn btn-primary">Thêm</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="editYoutube" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Sửa link Youtube</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'admin/editYoutube','id' => 'editYoutube')) !!}
                          <div class="modal-body">
	                        <input id="idEdit" class="form-control" name="id" type="hidden" required />
                            <ul>
                            <li class="info-cell">
                                <p>Link Youtube: </p>
                                <input id="inputEdit" class="form-control" name="name" type="text" required />
                            </li>
                        </ul>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button id="change" type="submit" class="btn btn-primary">Sửa</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
						<div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="30%">ID</th>
                                    <th width="40%">Tiêu Đề</th>
                                    <th width="10%">Thao Tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < count($youtube); $i++)
                                    <tr>
                                        <td width="30%"><a href="edit/{{ $youtube[$i]->id }}"> {{ $youtube[$i]->id }}</a></td>
                                        <td width="40%">{{ $youtube[$i]->link }}</td>
                                        <td width="10%"><a onclick="return admin.confirmDelete('Bạn muốn xóa link Youtube này ?')" href='/admin/delYoutube/{{ $youtube[$i]->id }}' style='margin-bottom: 0px' class='btn btn-danger btn-circle delete'>
                                                <i class='fa fa-times'></i>
                                            </a>
                                            <button style='margin-bottom: 0px' class='btn btn-circle delete' data-toggle="modal" data-target="#editYoutube" onclick='currentValue({{$youtube[$i]->id}},"{{ $youtube[$i]->link }}")'>
                                                <i class='fa fa-pencil'></i>
                                            </button></td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                            <div id="paging" style="text-align:center;">
                                {!! $youtube->render() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
@endsection 
@section('page-script')
<script src="{{ url('/').'/js/plugins/metisMenu/jquery.metisMenu.js'}}"></script>
<script src="{{ url('/').'/js/plugins/slimscroll/jquery.slimscroll.min.js'}}"></script>
<script src="{{url('/').'/js/plugins/pace/pace.min.js'}}"></script>
<script type="text/javascript">
function currentValue(id,name){
	$('#inputEdit').val(name);
	$('#idEdit').val(id);
}
</script>
@endsection