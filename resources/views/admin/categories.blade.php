@extends('layouts.app') 
@section('title', 'Main page') 
@section('css')
<link href="{{ url('/').'/css/index.css' }}" rel="stylesheet"> 
<link href="{{ url('/').'/css/staff.css' }}" rel="stylesheet"> 
<style type="text/css">
td img {
    max-width: 50%;
}
</style>
@endsection 
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách danh mục</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    	 <div class="row">
                            <div class="col-sm-5 m-b-xs">
                            </div>
                            <div class="col-sm-4 m-b-xs">
                            </div>
                            <div class="col-sm-3">
                                <button data-toggle="modal" data-target="#addCategory" style="float: right;" class="btn btn-info manage" data-action="new" type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span style="font-size: 13px">Thêm danh mục</span></button>
                            </div>
                        </div>
                        <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Thêm danh mục</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'admin/addCategory','id' => 'addCategory','files'=>true)) !!}
                          <div class="modal-body">
                            <ul>
                            <li class="info-cell">
                                <p>Tên danh mục: </p>
                                <input class="form-control" name="name" type="text" required />
                            </li>
                            <li class="info-cell">
                                <p>Ảnh: </p>
                                <input type="file" ID="imgPre" name="photo" />
                            </li>
                            <li class="info-cell">
                                <p>Loại: </p>
                            <select name="type">
                              <option value="Food">Food</option>
                              <option value="News">News</option>
                            </select>                            
                          </li>
                        </ul>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button id="change" type="submit" class="btn btn-primary">Thêm</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Sửa danh mục</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'admin/editCategory','id' => 'editCategory','files'=>true)) !!}
                          <div class="modal-body">
	                        <input id="idEdit" class="form-control" name="id" type="hidden" required />
                            <ul>
                            <li class="info-cell">
                                <p>Tên danh mục: </p>
                                <input id="inputEdit" class="form-control" name="name" type="text" required />
                            </li>
                            <li class="info-cell">
                                <p>Ảnh: </p>
                                <input type="file" ID="imgPre" name="photo" />
                            </li>
                        </ul>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button id="change" type="submit" class="btn btn-primary">Sửa</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
						<div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="30%">Image</th>
                                    <th width="30%">Tiêu Đề</th>
                                    <th width="20%">Type</th>
                                    <th width="10%">Thao Tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < count($categories); $i++)
                                    <tr>
                                        <td width="10%"><a href="edit/{{ $categories[$i]->id }}"> {{ $categories[$i]->id }}</a></td>
                                        <td width="30%">
                                          @if($categories[$i]->img != 'noImg')
                                          <img src="{{ url('/').'/'.$categories[$i]->img }}">
                                          @else
                                          <img src="{{ url('/').'/img/noImage.png'}}">
                                          @endif
                                        </td>
                                        <td width="30%">{{ $categories[$i]->name }}</td>
                                        <td width="20%">{{ $categories[$i]->type }}</td>
                                        <td width="10%"><a onclick="return admin.confirmDelete('Bạn muốn xóa danh mục này ?')" href='/admin/delCategory/{{ $categories[$i]->id }}' style='margin-bottom: 0px' class='btn btn-danger btn-circle delete'>
                                                <i class='fa fa-times'></i>
                                            </a>
                                            <button style='margin-bottom: 0px' class='btn btn-circle delete' data-toggle="modal" data-target="#editCategory" onclick='currentValue({{$categories[$i]->id}},{{$categories[$i]->name}})'>
                                                <i class='fa fa-pencil'></i>
                                            </button></td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                            <div id="paging" style="text-align:center;">
                                {!! $categories->render() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
@endsection 
@section('page-script')
<script src="{{ url('/').'/js/plugins/metisMenu/jquery.metisMenu.js'}}"></script>
<script src="{{ url('/').'/js/plugins/slimscroll/jquery.slimscroll.min.js'}}"></script>
<script src="{{url('/').'/js/plugins/pace/pace.min.js'}}"></script>
<script type="text/javascript">
function currentValue(id,name){
	$('#inputEdit').val(name);
	$('#idEdit').val(id);
}
</script>
@endsection