@extends('layouts.app') 
@section('title', 'Main page') 
@section('css')
<link href="{{ url('/').'/css/index.css' }}" rel="stylesheet"> 
<link href="{{ url('/').'/css/staff.css' }}" rel="stylesheet"> 
@endsection 
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
    	<div class="row border-bottom white-bg dashboard-header">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <div class="info">
                        <ul>
                            <li class="info-cell">
                                <p>Họ và tên: </p>
                                <span>{{Session::get('usernamelogin')->name}}</span>
                            </li>
                            <li class="info-cell">
                                <p>Email: </p>
                                <span>
                                    @if(Session::get('usernamelogin')->email != null)
                                    {{Session::get('usernamelogin')->email}}
                                    @else
                                    Không có
                                    @endif
                                </span>
                            </li>
                        </ul>
                    </div>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changeInfo">
                      Đổi thông tin
                    </button>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#changePass">
                      Đổi mật khẩu
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="changeInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Đổi thông tin</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'api/changeInfo','id' => 'changeInfo')) !!}
                          <div class="modal-body">
                            <ul>
                            <li class="info-cell">
                                <p>Họ và tên: </p>
                                <input name="name" class="form-control" value="{{Session::get('usernamelogin')->name}}" required />
                            </li>
                            <li class="info-cell">
                                <p>Email: </p>
                                <input required name="email" type="email" class="form-control" value="@if(Session::get('usernamelogin')->email != null){{Session::get('usernamelogin')->email}}@else Không có @endif" />                                
                            </li>
                            <li class="info-cell">
                                <p>Nhập mật khẩu: </p>
                                <input required type="password" name="passwordInfo" class="form-control" />
                            </li>
                        </ul>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button type="submit" class="btn btn-primary">Thay đổi</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h3 class="modal-title pull-left" id="exampleModalLabel">Đổi mật khẩu</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          {!! Form::open(array('url' => 'api/changePass','id' => 'changePass')) !!}
                          <div class="modal-body">
                            <ul>
                            <li class="info-cell">
                                <p>Mật khẩu cũ: </p>
                                <input class="form-control" name="oldPass" type="password" required />
                            </li>
                            <li class="info-cell">
                                <p>Mật khẩu mới: </p>
                                <input class="form-control" name="newPass" type="password" required/>
                            </li>
                            <li class="info-cell">
                                <p>Nhập lại mật khẩu mới: </p>
                                <input class="form-control" name="renewPass" type="password" required onkeyup="checkPass()"/>
                            </li>
                        </ul>
                        <div id="error-pass"></div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button id="change" type="submit" class="btn btn-primary">Thay đổi</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách bài viết</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    	 <div class="row">
                            <div class="col-sm-5 m-b-xs">
                            </div>
                            <div class="col-sm-4 m-b-xs">
                            </div>
                            <div class="col-sm-3">
                                <a href="admin/new" style="float: right;" class="btn btn-info manage" data-action="new" type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span style="font-size: 13px">Thêm Tin Tức</span></a>
                            </div>
                        </div>
						<div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="30%">Tiêu Đề</th>
                                    <th width="40%">Mô Tả Ngắn</th>
                                    <th width="10%">Thao Tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i = 0; $i < count($lstBlogs); $i++)
                                    <tr>
                                        <td width="30%"><a href="edit/{{ $lstBlogs[$i]->id }}"> {{ $lstBlogs[$i]->title }}</a></td>
                                        <td width="40%">{{ $lstBlogs[$i]->description }}</td>
                                        <td width="10%"><a onclick="return admin.confirmDelete('Bạn muốn xóa tin tức này ?')" href='admin/delete/{{ $lstBlogs[$i]->id }}' style='margin-bottom: 0px' class='btn btn-danger btn-circle delete'>
                                                <i class='fa fa-times'></i>
                                            </a></td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                            <div id="paging" style="text-align:center;">
                                {!! $lstBlogs->render() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
@endsection 
@section('page-script')
<script src="{{ url('/').'/js/plugins/metisMenu/jquery.metisMenu.js'}}"></script>
<script src="{{ url('/').'/js/plugins/slimscroll/jquery.slimscroll.min.js'}}"></script>
<script src="{{url('/').'/js/plugins/pace/pace.min.js'}}"></script>
<script type="text/javascript">
$("#changeInfo").on('submit', function(event) {
    event.preventDefault(); 


    var token = $('input[name="csrf-token"]').val();
    var name = $("input[name=name]").val();
    var email = $('input[name="email"]').val();
    var passConfirm = $('input[name="passwordInfo"]').val();
    $.ajax({

        type:'POST',
        url:"{{url('api/changeInfo')}}/"+name+'/'+email+'/'+passConfirm,
        dataType: 'JSON',
        data: {
            "_method": 'POST',
            "_token": token
        },
        success:function(data){
          if(data.success == true){
            console.log(data);
            toastr.success("Thay đổi thông tin thành công !", "Chúc mừng");
            window.setTimeout(function(){ window.location = "{{url('/admin')}}"; },1500);
          }
          else{
            toastr.error(data.error, "Thất bại");
          }
        },
        error:function(data){
          toastr.error(data.error, "Thất bại");
        },
    });
});
$("#changePass").on('submit', function(event) {
    event.preventDefault(); 


    var token = $('input[name="csrf-token"]').val();
    var oldPass = $("input[name=oldPass]").val();
    var newPass = $('input[name="newPass"]').val();
    var renewPass = $('input[name="renewPass"]').val();
    $.ajax({

        type:'POST',
        url:"{{url('api/changePass')}}/"+oldPass+'/'+newPass+'/'+renewPass,
        dataType: 'JSON',
        data: {
            "_method": 'POST',
            "_token": token
        },
        success:function(data){
          if(data.success == true){
            console.log(data);
            toastr.success("Thay đổi thông tin thành công !", "Chúc mừng");
            window.setTimeout(function(){ window.location = "{{url('/admin')}}"; },1500);
          }
          else{
            toastr.error(data.error, "Thất bại");
          }
        },
        error:function(data){
          toastr.error(data.error, "Thất bại");
        },
    });
});
</script>
<script type="text/javascript">
  function checkPass() {
    var newPass = $('input[name="newPass"]').val();
    var renewPass = $('input[name="renewPass"]').val();
    if(newPass != renewPass){
        document.getElementById("error-pass").innerHTML = "Mật khẩu chưa khớp";
        document.getElementById("error-pass").style.color = "red";
        document.getElementById("change").disabled = true;
    }
    else {
        document.getElementById("error-pass").innerHTML = "Mật khẩu khớp";
        document.getElementById("error-pass").style.color = "green";
        document.getElementById("change").disabled = false;
    }
  }

</script>
@endsection