<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>GroupT.com | Login</title>

    <link href="{{url('/').'/css/plugins/toastr/toastr.min.css'}}" rel="stylesheet">

    <link href="{{ url('/').'/css/bootstrap.min.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/font-awesome/css/font-awesome.css' }}" rel="stylesheet">

    <link href="{{ url('/').'/css/animate.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/style.css' }}" rel="stylesheet">
    <style type="text/css">
  		.logo-custom>h1 {
  	    font-size: 60px;
  	    color: #2196F3;
  	    letter-spacing: -3px;
  		}
      .logo h2 {
		    font-size: 45px;
		}
    </style>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div class="logo-custom">

                <div class="logo">
                  <h2>TGroup Admin</h2>
                </div>

            </div>

            {!! Form::open(array('url' => '/admin/login')) !!}
                <div class="form-group">
                   {!! Form::text('email','',array('class' => 'form-control', 'placeholder' => 'Email', 'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password',array('class' => 'form-control', 'placeholder' => 'Password', 'required')) !!}
                </div>
                <p style="color: red; font-weight: bold"><?php if(isset($error)) echo $error ?></p>
                <button type="submit" class="btn btn-primary block full-width m-b login-btn">Login</button>

                <a href="#"><small>Forgot Password?</small></a>
            {!! Form::close() !!}
			 <p class="m-t"> <small><i class="fa fa-chevron-left"></i> <a href="{{url('/')}}" style="color: #676a6c">Back to Home Page</a></small> </p>
            <p class="m-t"> <small>Copyright &copy; 2018. All Rights Reserved by GroupT.Com</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('public/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>

</body>

</html>
