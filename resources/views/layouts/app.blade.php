<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TGroup.com | @yield('title') </title>

    <link rel="stylesheet" href="{{ url('/').'/css/vendor.css' }}" />
    <link rel="stylesheet" href="{{ url('/').'/css/app.css' }}" />
    @yield('js')

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="{{ url('/').'/css/font-awesome/css/font-awesome.css' }}" rel="stylesheet">

    <link href="{{ url('/').'/css/animate.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/style.css' }}" rel="stylesheet">

        <!-- Sweet Alert -->
    <link href="{{url('/').'/css/plugins/sweetalert/sweetalert.css'}}" rel="stylesheet">
    
    <!-- Toastr style -->
    <link href="{{url('/').'/css/plugins/toastr/toastr.min.css'}}" rel="stylesheet">

    @yield('css')


</head>
<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->



<script src="{{ url('/').'/js/jquery-2.1.1.js' }}" type="text/javascript"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script src="{{ url('/').'/js/underscore.js' }}" type="text/javascript"></script>

    <!-- Sweet alert -->
    <script src="{{ url('/').'/js/plugins/sweetalert/sweetalert.min.js' }}"></script>

    <script src="{{ url('/').'/js/plugins/toastr/toastr.min.js' }}"></script>

    <script type="text/javascript">
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": false,
        "preventDuplicates": true,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
    </script>

@yield('page-script')
@section('scripts')
@show

</body>
</html>
