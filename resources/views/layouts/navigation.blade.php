
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Session::get('usernamelogin')->name }}</strong>
                            </span> <span class="text-muted text-xs block">Example menu <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{url('/').'/logout'}}">Đăng xuất</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    TGroup
                </div>
            </li>
            <li class="{{ isActiveRoute('main') }}">
                <a href="{{url('/').'/admin'}}">
                    <i class="fa fa-th-large"></i> 
                    <span class="nav-label">Thông tin tài khoản</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('main') }}">
                <a href="{{url('/').'/admin/categories'}}">
                    <i class="fa fa-th-large"></i> 
                    <span class="nav-label">Danh mục</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('main') }}">
                <a href="{{url('/').'/admin/youtube'}}">
                    <i class="fa fa-th-large"></i> 
                    <span class="nav-label">Youtube</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('logout') }}">
                <a href="{{url('/').'/logout'}}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> 
                    <span class="nav-label">Đăng xuất</span> 
                </a>
            </li>
        </ul>

    </div>
</nav>
