<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
      <title>CookWithTGroup</title>
      <link rel="icon" href="media/images/favicon.ico">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="libraries/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
      <link rel="stylesheet" href="libraries/css/style.css">
    </head>
    <body>
        <!--hero section-->
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2 text-center">
                                    <h2>You have successfully registered and logged in.</h2>
                                    <p>You will be redirected to home page. Click <a href="{{url('/')}}">here</a> if you are not redirected in 3 seconds. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<script>
    setTimeout(function(){location.href="{{url('/')}}"} , 3000);
</script>
    </body>
</html>
