<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
      <title>CookWithTGroup</title>
      <link rel="icon" href="media/images/favicon.ico">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="libraries/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
      <link rel="stylesheet" href="libraries/css/style.css">
    </head>
    <body>
        <!--hero section-->
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2 text-center">
                                    <h2>Create Your Account</h2>
                                </div>
                                <p class="mt-4 text-white lead text-center">
                                    Sign up to get started with CookWithTGroup
                                </p>
                                <div class="mt-4">
                                    <form action="{{url('/').'/register'}}" method="post">
                                      {{ csrf_field() }}
                                        <div class="form-group">
                                            <input required type="name" class="form-control" name="name" value="" placeholder="Enter your name">
                                        </div>

                                        <div class="form-group">
                                            <input required type="email" class="form-control" name="email" value="" placeholder="Enter email address">
                                        </div>
                                        <div class="form-group">
                                            <input required type="password" id="password" class="form-control" name="password" value="" placeholder="Enter password">
                                        </div>
                                        <div class="form-group">
                                            <input required type="password" id="confirm_password" class="form-control" name="confirm_password" onkeyup="checkPass()" value="" placeholder="Confirm password">
                                        </div>
                                        <div id="error" class="register-error">
                                            <p id="error-pass" style="color: red; font-weight: bold;">
                                                
                                            </p>
                                        </div>

                                        <p style="color: red; font-weight: bold"><?php if(isset($error)) echo $error ?></p>
                                        <button id="Dang-ky" type="submit" class="btn btn-primary btn-block">Create Account</button>
                                    </form>
                                    <div class="clearfix"></div>
                                    <p class="content-divider center mt-4"><span>or</span></p>
                                </div>
                                <!-- <p class="mt-4 social-login text-center">
                                    <a href="https://twitter.com/" class="btn btn-twitter"><em class="ion-social-twitter"></em></a>
                                    <a href="https://facebook.com" class="btn btn-facebook"><em class="ion-social-facebook"></em></a>
                                    <a href="https://plus.google.com/people" class="btn btn-google"><em class="ion-social-googleplus"></em></a>
                                
                                </p> -->
                                <p class="text-center">
                                    Already have an account? <a href="{{url('/').'/login'}}">Login Now</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<script>
function checkPass() {
    var password = document.getElementById("password").value;
    var rePassword = document.getElementById("confirm_password").value;
    if(password != rePassword){
        document.getElementById("error-pass").innerHTML = "Mật khẩu chưa khớp";
        document.getElementById("error-pass").style.color = "red";
        document.getElementById("Dang-ky").disabled = true;
    }
    else {
        document.getElementById("error-pass").innerHTML = "Mật khẩu khớp";
        document.getElementById("error-pass").style.color = "green";
        document.getElementById("Dang-ky").disabled = false;
    }
}
</script>

    </body>
</html>
