@extends('layoutFrontend.app') 
@section('title', 'Home Page') 
@section('heroBackground')
{{$blog->title}}
@endsection 
@section('content')
<style type="text/css">
blockquote {
    font-size: 32px;
    border-top: 1px solid #f2f3f8;
    border-bottom: 1px solid #f2f3f8;
    color:#f9b129;
}
.offset-top-40 {
    color: black;
}
</style>
                <section>
                  <article class="post post-classic">
                    <!-- Post media--><img src="{{url('/').'/'.$blog->img}}" alt="" width="870" height="412" class="img-responsive">
                    <!-- Post content-->
                    <section class="post-content text-left offset-top-25">
                      <h5 class="text-uppercase">{{$blog->title}}</h5>
                      <ul class="list-inline list-inline-md offset-top-5">
                        <li>
                          <div class="unit unit-horizontal unit-spacing-xxs">
                            <div class="unit-left"><span class="text-base">Date:</span></div>
                            <div class="unit-body">
                              <time datetime="2016-01-01">{{$blog->created_at}}</time>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="unit unit-horizontal unit-spacing-xxs">
                            <div class="unit-left"><span class="text-base">Posted by:</span></div>
                            <div class="unit-body"><a href="#" class="link link-gray-light">{{$blog->author}}</a></div>
                          </div>
                        </li>
                        <li>
                          <div class="unit unit-horizontal unit-spacing-xxs">
                            <div class="unit-left"><span class="text-base">Comments:</span></div>
                            <div class="unit-body"><a href="#" class="link link-gray-light">2</a></div>
                          </div>
                        </li>
                        <li>
                          <div class="unit unit-horizontal unit-spacing-xxs">
                            <div class="unit-left"><span class="text-base">Category:</span></div>
                            <div class="unit-body"><a href="#" class="link link-gray-light">{{$blog->category}}</a></div>
                          </div>
                        </li>
                      </ul>
                      <hr class="offset-top-15">
                      <div class="offset-top-20">
                        <p class="text-base">{{$blog->description}}</p>
                      </div>

                      <div class="offset-top-40">
                        <p class="text-base">{!!$blog->detail!!}</p>
                      </div>
                    </section>
                    <footer class="offset-top-50 text-sm-left clearfix">
                      <div class="big text-bold text-base pull-sm-left">Share this post:</div>
                      <ul class="list-inline pull-sm-right offset-top-0 text-sm-right">
                        <li><a href="##" class="link-darkest icon icon-xxs-mod-1 fa fa-facebook"></a></li>
                        <li><a href="##" class="link-darkest icon icon-xxs-mod-1 fa fa-twitter"></a></li>
                        <li><a href="##" class="link-darkest icon icon-xxs-mod-1 fa fa-google-plus"></a></li>
                        <li><a href="##" class="link-darkest icon icon-xxs-mod-1 fa fa-pinterest"></a></li>
                      </ul>
                    </footer>
                  </article>
                  <hr class="offset-top-50">
                  <h5 class="offset-top-60 offset-sm-top-90 text-uppercase text-left">Related Posts</h5>
                  <div class="range range-sm-justify">
                  	@for($i = 0; $i < count($related); $i++)
                    <div class="cell-sm-6">
                                      <!-- Post Classic-->
                                      <article class="post post-classic post-widget">
                                        <!-- Post media--><img src="{{url('/').'/'.$related[$i]->img}}" alt="" width="398" height="269" class="img-responsive">
                                        <!-- Post content-->
                                        <section class="post-content text-left offset-top-25">
                                          <h6><a href="#" class="link-default">{{$related[$i]->title}}</a></h6>
                                          <ul class="list-inline list-inline-sm offset-top-5">
                                            <li>
                                              <div class="unit unit-horizontal unit-spacing-xxs">
                                                <div class="unit-left"><span class="text-base">Date:</span></div>
                                                <div class="unit-body">
                                                  <time datetime="2016-01-01">{{$related[$i]->created_at}}</time>
                                                </div>
                                              </div>
                                            </li>
                                            <li>
                                              <div class="unit unit-horizontal unit-spacing-xxs">
                                                <div class="unit-left"><span class="text-base">Comments:</span></div>
                                                <div class="unit-body"><span>2</span></div>
                                              </div>
                                            </li>
                                            <li>
                                              <div class="unit unit-horizontal unit-spacing-xxs">
                                                <div class="unit-left"><span class="text-base">Posted by:</span></div>
                                                <div class="unit-body"><span>{{$related[$i]->author}}</span></div>
                                              </div>
                                            </li>
                                          </ul>
                                        </section>
                                      </article>
                    </div>
                    @endfor
                  </div>
                </section>
@endsection