    <link href="{{ url('/').'/css/animate.css' }}" rel="stylesheet">
<!DOCTYPE html>
<html lang="vn">
<head>
  <meta charset="UTF-8">
  <title>CookWithTGroup</title>
  <link rel="icon" href="media/images/favicon.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="libraries/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
  <link rel="stylesheet" href="libraries/css/style.css">
      <link href="{{ url('/').'/css/animate.css' }}" rel="stylesheet">
</head>
<body>

          <!--hero section-->
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <div class="card border-none">
                            <div class="card-body">
                                <div class="mt-2">
                                    <img src="media/images/member.png" class="brand-logo mx-auto d-block img-fluid rounded-circle"/>
                                </div>
                                <p class="mt-4 text-white lead text-center">
                                    Sign in to access your Authority account
                                </p>
                                <div class="mt-4">
                                    <form action="{{url('/')}}" method="post">
                                      {{ csrf_field() }}
                                        <div class="form-group">
                                            <input required name="email" type="email" class="form-control" id="email" value="" placeholder="Enter email address">
                                        </div>
                                        <div class="form-group">
                                            <input required name="password" type="password" class="form-control" id="password" value="" placeholder="Enter password">
                                        </div>
                                        <p style="color: red; font-weight: bold"><?php if(isset($error)) echo $error ?></p>
                                        <label class="custom-control custom-checkbox mt-2">
                                            <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description text-white">Keep me logged in</span>
                                        </label>
                                        <button type="submit" class="btn btn-primary float-right">Sign in</button>
                                    </form>
                                    <div class="clearfix"></div>
                                    <p class="content-divider center mt-4"><span>or</span></p>
                                </div>
                                <!-- <p class="mt-4 social-login text-center">
                                    <a href="https://twitter.com/" class="btn btn-twitter"><em class="ion-social-twitter"></em></a>
                                    <a href="https://facebook.com" class="btn btn-facebook"><em class="ion-social-facebook"></em></a>
                                    <a href="https://plus.google.com/people" class="btn btn-google"><em class="ion-social-googleplus"></em></a>
                                
                                </p> -->
                                <p class="text-center">
                                    Don't have an account yet? <a href="{{url('/').'/register'}}">Sign Up Now</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</body>
</html>