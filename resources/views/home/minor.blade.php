@extends('layouts.app')

@section('title', 'Minor page')
@section('css')
<link href="{{ url('/').'/css/plugins/summernote/summernote.css' }}" rel="stylesheet">
<link href="{{ url('/').'/css/plugins/summernote/summernote-bs3.css' }}" rel="stylesheet">
<style type="text/css">
input.form-control {
    margin-bottom: 5px;
    margin-top: 5px;
}
</style>
@endsection
@section('content')
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
          <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
              <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                      <h5>Đăng bài viết</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="responsive">
                          <div class="row">
                          <div class="form-group"><label style="text-align:right;margin-top:5px;" class="col-sm-2 control-label">Tiêu đề: </label>
                            <div class="col-sm-10"><input type="text" class="form-control"></div>
                          </div>
                          <div class="form-group"><label style="text-align:right;margin-top:5px;" class="col-sm-2 control-label">Nội dung: </label>
                            <div class="col-sm-10"><div class="summernote">
                            <h3>Lorem Ipsum is simply</h3>
                            dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                            typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                            <br/>
                            <br/>
                            <ul>
                                <li>Remaining essentially unchanged</li>
                                <li>Make a type specimen book</li>
                                <li>Unknown printer</li>
                            </ul>
                        </div></div>
                          </div>
                          <div class="form-group"><label style="text-align:right;margin-top:5px;" class="col-sm-2 control-label">Tags: </label>
                            <div class="col-sm-10"><input type="text" class="form-control"></div>
                          </div>
                          <div class="form-group"><label style="text-align:right;margin-top:5px;" class="col-sm-2 control-label">Danh mục: </label>
                            <div class="col-sm-10"><input type="text" class="form-control"></div>
                          </div>
                        </div>
                        <div style="text-align:center;margin-top:5px;">
                            <button id="change" type="submit" class="btn btn-primary">Đăng bài</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('page-script')
    <!-- SUMMERNOTE -->
    <script src="{{url('/').'/js/plugins/summernote/summernote.min.js'}}"></script>

    <script>
        $(document).ready(function(){

            $('.summernote').summernote();

       });
    </script>
@endsection