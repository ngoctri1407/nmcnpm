<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TGroup.com | @yield('title') </title>
    <link rel="stylesheet" href="{{ url('/').'/css/vendor.css' }}" />
    <link rel="stylesheet" href="{{ url('/').'/css/app.css' }}" /> @yield('js')
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="{{ url('/').'/css/font-awesome/css/font-awesome.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/animate.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/style.css' }}" rel="stylesheet">
    <link href="{{ url('/').'/css/frontEnd.css' }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{url('/').'/css/plugins/sweetalert/sweetalert.css'}}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{url('/').'/css/plugins/toastr/toastr.min.css'}}" rel="stylesheet"> @yield('css')
</head>
<div class="row products" id="block-main">
    <div class="col-md-3 product-wrapper  _tracking" id="product-344730-wrapper">
        <div class="product product-kind-1">
            <div class="product__image">
                <a href="">
                                <img itemprop="image" class="lazy b-loaded" width="280" height="auto" src="https://static.hotdeal.vn/images/1509/1509160/280x280/344730-uu-dai-gio-vang-buffet-bang-chuyen-free-buffet-kem-tokyo-ginza-koma.jpg">
                   <div class="item__meta">
                    <span class="view">Xem Ngay</span>
                </div>
            </a>
                <div class="item__delivery"><i class="hd hd-evoucher"></i> E-Voucher</div>
                <div class="item__location"><span class="glyphicon glyphicon-map-marker"></span> Quận 1 </div>
                
            </div>
            <div class="product__header">
                <h3 class="product__title">
                <a href="" itemprop="name" title="Ưu Đãi Giờ Vàng: Buffet Băng Chuyền, Free Buffet Kem - Tokyo Ginza Koma">Ưu Đãi Giờ Vàng: Buffet Băng Chuyền, Free Buffet Kem - Tokyo Ginza Koma</a>
                <meta itemprop="brand" content="">
            </h3>
            </div>
            <div class="product__info">
                <div class="product__stats">
                    
                    <div class="product__views">
                        <i class="hd hd-user"></i> 303 </div>
                </div>
            </div>
        </div>
    </div>
</div>