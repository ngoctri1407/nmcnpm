@extends('layoutFrontend.app') 
@section('title', 'Home Page') 
@section('heroBackground', 'Cook with TGroup') 
@section('content')
                <section>
                  @for($i = 0; $i < count($blogs); $i++)

                                 <div class="offset-top-60 offset-sm-top-100">
                                    <!-- Post Classic-->
                                    <article class="post post-classic">
                                      <!-- Post media--><img src="{{$blogs[$i]->img}}" alt="" width="870" height="412" class="img-responsive">
                                      <!-- Post content-->
                                      <section class="post-content text-left offset-top-25">
                                        <h5><a href="{{url('/').'/detail/'.$blogs[$i]->id}}" class="link-default text-uppercase">{{$blogs[$i]->title}}</a></h5>
                                        <ul class="list-inline list-inline-md offset-top-5">
                                          <li>
                                            <div class="unit unit-horizontal unit-spacing-xxs">
                                              <div class="unit-left"><span class="text-base">Date:</span></div>
                                              <div class="unit-body">
                                                <time datetime="2016-01-01">{{$blogs[$i]->created_at}}</time>
                                              </div>
                                            </div>
                                          </li>
                                          <li>
                                            <div class="unit unit-horizontal unit-spacing-xxs">
                                              <div class="unit-left"><span class="text-base">Posted by:</span></div>
                                              <div class="unit-body"><a href="#" class="link link-gray-light">{{$blogs[$i]->author}}</a></div>
                                            </div>
                                          </li>
                                          <li>
                                            <div class="unit unit-horizontal unit-spacing-xxs">
                                              <div class="unit-left"><span class="text-base">Comments:</span></div>
                                              <div class="unit-body"><a href="#" class="link link-gray-light">2</a></div>
                                            </div>
                                          </li>
                                          <li>
                                            <div class="unit unit-horizontal unit-spacing-xxs">
                                              <div class="unit-left"><span class="text-base">Category:</span></div>
                                              <div class="unit-body"><a href="#" class="link link-gray-light">{{$blogs[$i]->category}}</a>
                                              </div>
                                            </div>
                                          </li>
                                        </ul>
                                        <hr class="offset-top-15">
                                        <div class="offset-top-20">
                                          <p class="text-base">{{$blogs[$i]->description}}</p>
                                        </div>
                                        <div class="offset-top-35"><a href="{{url('/').'/detail/'.$blogs[$i]->id}}" class="btn btn-shape-circle btn-burnt-sienna-outline">Read More</a></div>
                                      </section>
                                    </article>
                  </div>
                  @endfor
                </section>
@endsection