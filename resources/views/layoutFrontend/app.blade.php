<!DOCTYPE html>
<!-- saved from url=(0063)# -->
<html lang="en" class="wide wow-animation smoothscroll desktop landscape rd-navbar-static-linked"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Site Title-->
    <title>TGroup.com | @yield('title')</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <link rel="icon" href="https://livedemo00.template-help.com/wt_61177/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="{{url('/').'/Classic_Blog_files'}}/css">
    <link rel="stylesheet" href="{{url('/').'/Classic_Blog_files'}}/style.css">
        <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
<script src="https://use.fontawesome.com/841f910dad.js"></script>
<style type="text/css">
.bg-image-breadcrumbs {
    background-image: url("{{url('/').'/Classic_Blog_files'}}/bg-image-breadcrumbs.jpg") !important;
}</style>
  </head>
  <body>
    <div class="page">
      
    </div>
    <!-- Page-->
    <div class="page text-center">
      <header class="page-head">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap rd-navbar-minimal" style="height: 108px;">
          <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="100px" data-lg-stick-up-offset="100px" class="rd-navbar rd-navbar-original rd-navbar-static">
            <div class="shell shell-fluid">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle toggle-original"><span></span></button>
                  <!-- RD Navbar Brand--><a href="{{url('/')}}" class="rd-navbar-brand brand">
                    <div class="brand-logo">
                      <svg x="0px" y="0px" width="157px" height="60px" viewBox="0 0 157 41">
                        <text transform="matrix(1 0 0 1 1.144409e-004 32)" fill="#2C2D2F" font-family="&#39;Grand Hotel&#39;" font-size="45.22">TGroup</text>
                        <path fill-rule="evenodd" clip-rule="evenodd" fill="#EB5453" d="M43.743,2.954c2.606,0,4.719,2.091,4.719,4.672  c0,2.58-2.113,4.672-4.719,4.672c-2.606,0-4.719-2.091-4.719-4.672C39.024,5.045,41.137,2.954,43.743,2.954z"></path>
                      </svg>
                    </div></a>
                </div>
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-nav-wrap toggle-original-elements">
                  <!-- RD Navbar Nav-->
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li class="rd-navbar--has-dropdown rd-navbar-submenu"><a href="#" class="navbar-icon">Menu</a>
                      <!-- RD Navbar Dropdown-->
                      <ul class="rd-navbar-dropdown menu-img-wrap rd-navbar-open-right">
                        <?php $FoodCategories = getAllFoodCategories();?>
                        @for($i = 0; $i < count($FoodCategories); $i++)
                        <li class="menu-img"><a href="#"><img src="{{url('/').'/'.$FoodCategories[$i]->img}}" alt="" width="88" height="60"><span>{{$FoodCategories[$i]->name}}</span></a></li>
                        @endfor
                      </ul>
                    </li>

                    <li class="active rd-navbar--has-dropdown rd-navbar-submenu"><a href="#" class="navbar-icon">News</a>
                      <ul class="rd-navbar-dropdown rd-navbar-open-right">
                        <?php $NewsCategories = getAllNewsCategories();?>
                        @for($i = 0; $i < count($NewsCategories); $i++)
                        <li><a href="">{{$NewsCategories[$i]->name}}</a></li>
                        @endfor
                      </ul>
                    </li>
                    <li><a href="#" class="navbar-icon">Contacts</a></li>
                  </ul>

                  <!-- RD Navbar Shop-->
                  <ul class="rd-navbar-shop list-inline">
                    <li><a href="callto:#" class="unit unit-horizontal unit-middle unit-spacing-xxs link-default">
                        <div class="unit-left"><i class="fa fa-mobile fa-2x" aria-hidden="true"></i></div>
                        <div class="unit-body">
                          <address class="contact-info"><span class="text-bold big">+84 1648 546 441</span></address>
                        </div></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">
        <!-- Breadcrumbs & Page title-->
        <section class="text-center section-34 section-sm-60 section-md-top-100 section-md-bottom-105 bg-image bg-image-breadcrumbs">
          <div class="shell shell-fluid">
            <div class="range range-condensed">
              <div class="cell-xs-12 cell-xl-preffix-1 cell-xl-11">
                <p class="h3 text-white">@yield('heroBackground')</p>
                <!-- <ul class="breadcrumbs-custom offset-top-10">
                  <li><a href="#">Home</a></li>
                  <li class="active">Classic_Blog</li>
                </ul> -->
              </div>
            </div>
          </div>
        </section>
        <section class="section-50 section-sm-100">
          <div class="shell">
            <div class="range range-md-center range-lg-right">
              <div class="cell-md-9">
                
                <!-- Blog Classic-->

        @yield('content')



        </div>
              <div class="cell-md-3 offset-top-100 offset-md-top-0">
                <!-- Section Blog Modern-->
                <aside class="text-left">
                  <!-- Search Form-->
                                  <!-- RD Search Form-->
                                  <form method="GET" class="form-search rd-search">
                                    <div class="form-group">
                                      <label for="blog-sidebar-2-form-search-widget" class="form-label form-search-label form-label-sm rd-input-label">Search</label>
                                      <input id="blog-sidebar-2-form-search-widget" type="text" name="s" autocomplete="off" class="form-search-input form-control ">
                                    </div>
                                    <button type="submit" class="form-search-submit"><span class="mdi mdi-magnify"></span></button>
                                  </form>
                  <div class="range offset-top-55">
                    <div class="cell-xs-6 cell-md-12">
                      <!-- Category-->
                      <div class="h6 text-uppercase">Categories</div>
                      <ul class="list list-marked list-marked-burnt-sienna list-bordered offset-top-10">
                        <?php $NewsCategories = getAllNewsCategories();?>
                        @for($i = 0; $i < count($NewsCategories); $i++)
                        <li><a href="#" class="link-default">{{$NewsCategories[$i]->name}}   <span class="text-gray-light">(4)</span></a></li>
                        @endfor
                      </ul>
                    </div>
                    <div class="cell-xs-6 cell-md-12 offset-top-50 offset-xs-top-0 offset-md-top-50">
                      <!-- Archive-->
                      <div class="h6 text-uppercase">Popular posts</div>
                      <div class="offset-top-30">

                        <?php $newBlog = getNewBlog();?>
                        
                        @for($i = 0; $i < count($newBlog); $i++)
                        <div class="unit unit-horizontal unit-spacing-xs">
                          <div class="unit-left"><img src="{{url('/').'/'.$newBlog[$i]->img}}" alt="" width="70" height="70" class="img-rounded">
                          </div>
                          <div class="unit-body"><a href="{{url('/').'/detail/'.$newBlog[$i]->id}}" class="link-default"> {{$newBlog[$i]->title}}</a>
                            <div>
                              <time datetime="2016">{{$newBlog[$i]->created_at}}</time> <span>/</span> <span>1 Comment</span>
                            </div>
                          </div>
                        </div>
                        @endfor
                      </div>
                    </div>
                  </div>
                  <!-- Twitter Feed-->
                  <div class="h6 offset-top-50 text-uppercase">Twitter Feed</div>
                  <div class="offset-top-25">
                                    <div data-twitter-username="templatemonster" data-twitter-date-hours=" hours ago" data-twitter-date-minutes=" minutes ago" class="twitter">
                                      <div data-twitter-type="tweet" class="twitter-sm offset-top-18">
                                        <div data-date="text" class="twitter-date"></div>
                                        <div data-tweet="text" class="twitter-text text-dark"></div>
                                        <div data-screen_name="text" class="twitter-name text-bold h6 text-sbold offset-top-10"></div>
                                      </div>
                                      <div data-twitter-type="tweet" class="twitter-sm">
                                        <div data-date="text" class="twitter-date"></div>
                                        <div data-tweet="text" class="twitter-text text-dark"></div>
                                        <div data-screen_name="text" class="twitter-name text-bold h6 text-sbold offset-top-10"></div>
                                      </div>
                                    <span id="loading_tweet" style="display: none;">Loading...</span></div>
                  </div>
                  <!-- Tags-->
                  <div class="h6 offset-top-50 text-uppercase">Tags</div>
                  <div class="offset-top-20">
                                    <div class="group-xs">

                        <?php $allCategories = getAllCategories();?>
                        @for($i = 0; $i < count($allCategories); $i++)
                        <a href="#" class="btn btn-tags btn-default btn-shape-circle">{{$allCategories[$i]->name}}</a>
                        @endfor
                                    </div>
                  </div>
                </aside>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footers-->
      <footer class="page-foot text-sm-left">
        <section class="bg-gray-darker section-top-55 section-bottom-60">
          <div class="shell">
            <div class="range border-left-cell">
              <div class="cell-sm-6 cell-md-3 cell-lg-4"><a href="#" class="brand brand-inverse">
                  <svg x="0px" y="0px" width="157px" height="60px" viewBox="0 0 157 41">
                    <text transform="matrix(1 0 0 1 1.144409e-004 32)" fill="#2C2D2F" font-family="&#39;Grand Hotel&#39;" font-size="45.22">TGroup</text>
                    <path fill-rule="evenodd" clip-rule="evenodd" fill="#EB5453" d="M43.743,2.954c2.606,0,4.719,2.091,4.719,4.672  c0,2.58-2.113,4.672-4.719,4.672c-2.606,0-4.719-2.091-4.719-4.672C39.024,5.045,41.137,2.954,43.743,2.954z"></path>
                  </svg></a>
                <ul class="list-unstyled contact-info offset-top-5">
                  <li>
                    <div class="unit unit-horizontal unit-top unit-spacing-xxs">
                      <div class="unit-left"><span class="text-white">Address:</span></div>
                      <div class="unit-body text-left text-gray-light">
                        <p>227 Nguyen Van Cu , Quan 5 , TP HCM</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-horizontal unit-top unit-spacing-xxs">
                      <div class="unit-left"><span class="text-white">Email:</span></div>
                      <div class="unit-body"><a href="mailto:#" class="link-gray-light">admin@gmail.com</a></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0">
                <h4 class="text-uppercase">Our menu</h4>
                <ul class="list-tags offset-top-15">
                      <?php $FoodCategories = getAllFoodCategories();?>
                        @for($i = 0; $i < count($FoodCategories); $i++)
                        <li class="text-gray-light"><a href="" class="link-gray-light">{{$FoodCategories[$i]->name}}</a></li>
                        @endfor

                  
                 
                </ul>
              </div>
              <div class="cell-sm-10 cell-lg-5 offset-top-50 offset-md-top-0 cell-md-6">
                <h4 class="text-uppercase">newsletter</h4>
                <div class="offset-top-20">
                        <form data-form-output="form-output-global" data-form-type="subscribe" method="post" action="https://livedemo00.template-help.com/wt_61177/bat/rd-mailform.php" class="rd-mailform form-subscribe form-inline-flex-xs" novalidate="novalidate">
                          <div class="form-group">
                            <input placeholder="Your Email" type="email" name="email" data-constraints="@Required @Email" class="form-control form-control-has-validation form-control-last-child" id="regula-generated-529574"><span class="form-validation"></span>
                          </div>
                          <button type="submit" class="btn btn-burnt-sienna btn-shape-circle">Subscribe</button>
                        </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section-20 bg-white">
          <div class="shell">
            <div class="range range-xs-center range-sm-justify">
              <div class="cell-sm-5 offset-top-26 text-md-left">
                <p class="copyright">
                  TGroup
                  &nbsp;©&nbsp;<span id="copyright-year">2018</span>&nbsp;<br class="veil-sm"><a href="">Privacy Policy</a>
                </p>
              </div>
              <div class="cell-sm-4 offset-top-30 offset-sm-top-0 text-md-right">
                <ul class="list-inline list-inline-sizing-1">
                  <li><a href="#" class="link-silver-light"><span class="icon icon-xs fa-instagram"></span></a></li>
                  <li><a href="#" class="link-silver-light"><span class="icon icon-xs fa-facebook"></span></a></li>
                  <li><a href="#" class="link-silver-light"><span class="icon icon-xs fa-twitter"></span></a></li>
                  <li><a href="#" class="link-silver-light"><span class="icon icon-xs fa-google-plus"></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </section>

      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script type="text/javascript" async="" src="{{url('/').'/Classic_Blog_files'}}/ec.js"></script><script type="text/javascript" async="" src="{{url('/').'/Classic_Blog_files'}}/analytics.js"></script><script async="" src="{{url('/').'/Classic_Blog_files'}}/gtm.js"></script><script src="{{url('/').'/Classic_Blog_files'}}/core.min.js"></script>
    <script src="{{url('/').'/Classic_Blog_files'}}/script.js"></script>
  <noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
<a href="#" id="ui-to-top" class="ui-to-top fa fa-angle-up"></a></body><!-- Google Tag Manager --></html>